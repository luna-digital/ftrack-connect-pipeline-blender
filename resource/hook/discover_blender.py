# :coding: utf-8
# :copyright: Copyright (c) 2021 Luna Digital, Ltd.

import os
import sys
import ftrack_api
import logging
import functools

logger = logging.getLogger('ftrack_connect_pipeline_blender.discover')

plugin_base_dir = os.path.normpath(
    os.path.join(
        os.path.abspath(
            os.path.dirname(__file__)
        ),
        '..'
    )
)

'''
We need to cache the ftrack dependencies path in an environment variable
so we can apply it in Blender after launch.

Blender uses an internal Python `sys` module, not the system one.
'''
python_dependencies = os.path.join(plugin_base_dir, 'dependencies')
os.environ['FTRACK_PYTHON_DEPENDENCIES'] = python_dependencies
sys.path.append(python_dependencies)

def on_discover_pipeline_blender(session, event):
    from ftrack_connect_pipeline_blender import __version__ as integration_version

    data = {
        'integration': {
            'name': 'ftrack-connect-pipeline-blender',
            'version': integration_version
        }
    }

    return data

def on_launch_pipeline_blender(session, event):
    pipeline_blender_base_data = on_discover_pipeline_blender(session, event)

    blender_plugins_path = os.path.join(
        plugin_base_dir, 'resource', 'plugins'
    )

    blender_script_path = os.path.join(
        plugin_base_dir, 'resource', 'scripts'
    )

    # Discover plugins from definitions
    #definitions_plugin_hook = os.getenv("FTRACK_DEFINITION_PLUGIN_PATH")
    #plugin_hook = os.path.join(definitions_plugin_hook, 'blender', 'python')
    
    selection = event['data'].get('context', {}).get('selection', [])
    
    if selection:
        pass
        
    return pipeline_blender_base_data

def register(session):
    '''Subscribe to application launch events on *registry*.'''
    if not isinstance(session, ftrack_api.session.Session):
        return

    handle_discovery_event = functools.partial(
        on_discover_pipeline_blender,
        session
    )

    session.event_hub.subscribe(
        'topic=ftrack.connect.application.discover'
        ' and data.application.identifier=blender*'
        ' and data.application.version >= 2.83',
        handle_discovery_event, priority=40
    )

    handle_launch_event = functools.partial(
        on_launch_pipeline_blender,
        session
    )
    '''
    session.event_hub.subscribe(
        'topic=ftrack.connect.application.launch',
        #' and data.application.identifier=blender*'
        #' and data.application.version >= 2.83',
        handle_launch_event, priority=40
    )'''